# Q: How come line 32 instead of line 33 still passes 3/4 tests?
# python -m pytest tests/test_ifs_1.py

"""
Class: Customer
"""


class Customer:
    def __init__(self, name, club_member=False):
        self.name = name
        self.club_member = club_member
        self.purchases = []

    def is_club_member(self):
        return self.club_member

    def get_purchases(self):
        return self.purchases


"""
Requirements:

If a customer is _a club member_ or
is a first time customer,
then apply 10% discount to order.
"""

# complete this function based on the requirements
def calculate_discount(customer):
    # if customer.is_club_member or not customer.get_purchases:
    if customer.is_club_member() or not customer.get_purchases():
        return 10/100
    else:
        return 0



