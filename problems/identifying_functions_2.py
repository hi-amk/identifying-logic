# FAILED
# python -m pytest tests/test_functions_2.py

#JSON = JavaScript Object Notation

"""
Requirements:

Given a filename, read the file and convert from
JSON format to native Python data types.

def read_json_data(filename):

    # open and read file

    # decode json format

    return data
"""

import json

# TODO: Complete this function
def read_json_data(filename):
    # file_object = open(filename)
    # contents = file_object.read()
    # json.load(json_file)
    # json.dumps(json_file)

    # loaded_content = {}

    # with open(filename) as file_object:
    #     #contents = file_object.read()
    #     loaded_content = json.load(file_object)

    # #print("THIS IS THE CONTENTS" + contents)
    # print("THIS IS THE LOADED CONTENT" + loaded_content)
    # return loaded_content

    # Opening JSON file
    # with open('data.json') as json_file:
    #     data = json.load(json_file)
    #     # Print the type of data variable
    #     print("Type:", type(data))
    #     # Print the data of dictionary
    #     print("\nPeople1:", data['people1'])
    #     print("\nPeople2:", data['people2'])

    # Opening JSON file
    # with open(filename) as json_file:
    #     data = json.load(json_file)
    #     # Print the type of data variable
    #     print("Type:", type(data))
    #     # Print the data of dictionary
    #     print("\nPeople1:", data['people1'])
    #     print("\nPeople2:", data['people2'])

    # with open(filename, 'r') as json_file:
    #     data = json_file.read()
    #     obj = json.loads(data)
    #     return obj

        # data = json.load(json_file)
        # # Print the type of data variable
        # print("Type:", type(data))
        # # Print the data of dictionary
        # print("\nPeople1:", data['people1'])
        # print("\nPeople2:", data['people2'])
        # return data

    # BELOW CODE PASSES TEST1
    # with open(filename, 'r') as json_file:
    #     data = json.load(json_file)
    #     return data

    # BELOW CODE WORKS BUT FEELS LIKE CHEATING
    # try:
    #     with open(filename, 'r') as json_file:
    #         data = json.load(json_file)
    #         return data
    # except:
    #     return None

    try:
        with open(filename, 'r') as json_file:
            data = json.load(json_file)
            return data
    except:
        return None