#PASSED
"""
Requirements:

Our site will display temperature in both ℃ and ℉
and need to be able to convert between them.

def celsius_to_f(celsius):
    f = (celsius * 9/5) + 32
    return f

def f_to_celsius(f):
    celsius = (f - 32) * 5 / 9
    return celsius
"""

# TODO: Complete this function
def celsius_to_f(celsius):
    return (celsius * 9/5) + 32

# TODO: Complete this function
def f_to_celsius(f):
    return (f-32) * 5/9

